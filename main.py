import ssl
import urllib.request
import re
import sys
import os

from slack_sdk import WebhookClient


ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

def checkLatestAvailableChromeDriver():
    contents = urllib.request.urlopen("https://chromedriver.chromium.org/downloads", context=ctx).read()
    contents = contents.decode("utf-8")

    matchobj = re.search("ChromeDriver.+[0-9]\..*", contents)
    if matchobj:
     result = matchobj.group(0)
    else:
     result = ""
    result = result.replace("ChromeDriver ","")

    f = open("latestchromedriver.txt", "r")
    last_version = f.read()
    print("---" + last_version + "---")
    print("---" + result + "---")
    f.close()
    if last_version != result:
        f1 = open("latestchromedriver.txt", "w+")
        f1.write(result)
        f1.close()
        # sys.exit(-1)

def checkLatestChromeDriverRelease():
    contents = urllib.request.urlopen("https://chromedriver.storage.googleapis.com/LATEST_RELEASE", context=ctx).read()
    contents = contents.decode("utf-8")
    print(contents)
    f = open("releasedchromedriver.txt", "r")
    last_version = f.read()
    print("---" + last_version + "---")
    print("---" + contents + "---")
    f.close()
    if last_version != contents:
        f1 = open("releasedchromedriver.txt", "w+")
        f1.write(contents)
        f1.close()
        client = WebhookClient(os.getenv("SLACK_WEBHOOK_URL"))
        response = client.send(text="New chrome driver version " + str(contents))
        print(response.status_code)
        # sys.exit(-1)

checkLatestChromeDriverRelease()
